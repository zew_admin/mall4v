export const tableOption = {
  border: true,
  index: true,
  indexLabel: '序号',
  stripe: true,
  align: 'center',
  addBtn: false,
  editBtn: false,
  viewBtn: false,
  delBtn: false,
  props: {
    label: 'label',
    value: 'value'
  },
  column: [
    {
      label: 'ID',
      prop: 'cardId',
      search: true
    },
    {
      label: '卡号',
      prop: 'cardNumber',
      search: true
    }
  ]
}
